package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

func writeim(url string) error {
	res, err := http.Get(url)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	name := "meow.jpg"
	out, err := os.Create(name)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, res.Body)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	url := "https://cataas.com/cat"
	err := writeim(url)
	if err != nil {
		panic(err)
	}

	fmt.Println("meow")
}
